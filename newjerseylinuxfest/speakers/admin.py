from django.contrib import admin

# Register your models here.
from speakers.models import Speaker

class SpeakerAdmin(admin.ModelAdmin):
        pass
admin.site.register(Speaker, SpeakerAdmin)
