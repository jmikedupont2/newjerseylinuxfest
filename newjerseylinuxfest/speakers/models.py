from django.db import models

# Create your models here.
class Speaker(models.Model):
    status = models.CharField(max_length=8)
    github_user = models.CharField(max_length=128)
    
